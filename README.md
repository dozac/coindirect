# Spring Boot Booking app - Constraints
- The Implementation was strictly based on the requirements and no edge cases were taken into the consideration, like for example, 
a Bingo Hall can have multiple bingo games during the day. Therefore, when a booking is being made, the user should also input the 
details of the start Date/time of the game along with the seat (row/column).
- The data types for the [`bookingId`], [`row`] , [`column`] in the provided [`BookingDto`] and [`RequestBookingDto`] 
were type [`String`], therefore the implementation was done using the given types as per the requirements, 
without changing these to types to something more trivial, like [`int`] or [`long`] where is the case.
- HTTPS status and responses were returned strictly as per the requirements.

### How to run the Application
The application can be run from BookingSystemApplication.java 
- The booking endpoints are accessible http://localhost:8080/swagger-ui/index.html
- The H2 database console is accessible http://localhost:8080/h2-console/login.jsp

### Unit & Integration tests
- For Integration tests was implemented Karate (BDD) framework which is build on top of Cucumber-JVM 
and use the Gherkin standard. The implementation run and test all types of scenarios as provided in the requirements.

#### How to run Karate Integration tests
1. Start application BookingSystemApplication.java. This will start on port 8080
2. Run [`order-controller.feature`] under [`/src/test/java/order/order-controller.feature`]
3. All tests should run as show on the following image

![](screenshots/intellij-tests.png)
5. To view th UI formatted report of the test cases that have been executed!, 
navigate to the generated [`target`] folder of the src project as shown on the image below 

![](screenshots/karate-summary.png)
7. Double-click on the [`karate-summary.html`] file to open it in the browser. The report should be now visible in the browser as per the following img 

![](screenshots/karate-report.png)

**_NOTE:_ If you want to rerun the tests. Stop the application and follow again the above steps 1 -> 6
.By running the Integration tests multiple times without stopping the Application, the 
test scenarios results won't match the data in the H2 database**

#### How to run Unit tests
1. Run [`UtilsTest.java`] under [`src/test/java/com/coindirect/recruitment/util/UtilsTest.java`]
