package com.coindirect.recruitment.repository;

import com.coindirect.recruitment.model.Seat;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;


public interface SeatRepository extends CrudRepository<Seat, Long> {
    Optional<Seat> getSeatByRowAndColumn(final String row, final String column);
}
