package com.coindirect.recruitment.repository;

import com.coindirect.recruitment.model.Booking;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.UUID;


public interface BookingRepository extends CrudRepository<Booking, UUID> {
    Optional<Booking> findBySeatRowAndSeatColumn(String row, String column);
}
