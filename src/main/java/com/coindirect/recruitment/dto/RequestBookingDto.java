package com.coindirect.recruitment.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
public class RequestBookingDto {

    @NotBlank(message = "Name is mandatory")
    String name;

    @NotBlank(message = "Row is mandatory")
    @Size(min=1, max=1)
    @Pattern(regexp="^[A-C]*$",message = "Invalid Row input, only UpperCase Letter between A-C")
    String row;

    @NotBlank(message = "Column is mandatory")
    @Size(min=1, max=1)
    @Pattern(regexp="^[1-9]*$",message = "Invalid Column input, only numbers between 1 and 9")
    String column;
}
