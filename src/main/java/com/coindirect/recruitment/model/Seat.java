package com.coindirect.recruitment.model;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Set;


@Entity
@Getter@Setter
@Table(name="SEAT")
public class Seat {

    @Id
    @Column(name = "seat_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "seat_row")
    private String row;

    @Column(name = "seat_column")
    private String column;

    @Fetch(value= FetchMode.SELECT)
    @OneToMany(mappedBy = "seat")
    Set<Booking> bookings;

}
