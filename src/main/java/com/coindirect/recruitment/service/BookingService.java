package com.coindirect.recruitment.service;

import com.coindirect.recruitment.dto.BookingDto;
import com.coindirect.recruitment.dto.RequestBookingDto;
import com.coindirect.recruitment.exception.SeatNotFoundException;
import com.coindirect.recruitment.exception.UUIDNotValidException;
import com.coindirect.recruitment.model.Booking;
import com.coindirect.recruitment.model.Seat;
import com.coindirect.recruitment.repository.BookingRepository;
import com.coindirect.recruitment.repository.SeatRepository;
import com.coindirect.recruitment.util.Utils;
import io.vavr.control.Either;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

@Slf4j
@Service
public class BookingService {

    private final BookingRepository bookingRepository;
    private final SeatRepository seatRepository;
    private final ModelMapper modelMapper;


    public BookingService(BookingRepository bookingRepository, SeatRepository seatRepository, ModelMapper modelMapper) {
        this.bookingRepository = bookingRepository;
        this.seatRepository = seatRepository;
        this.modelMapper = modelMapper;
    }

    /**
     * Create new booking
     * @param requestBooking
     * @return booking details
     */
    @Transactional(isolation= Isolation.REPEATABLE_READ, propagation = Propagation.REQUIRES_NEW)
    public BookingDto addBooking(RequestBookingDto requestBooking) {
        Optional<Seat> seat = seatRepository.getSeatByRowAndColumn(requestBooking.getRow(), requestBooking.getColumn());
        if (seat.isPresent()) {
            Booking booking = this.modelMapper.map(requestBooking, Booking.class);
            booking.setSeat(seat.get());
            Booking persistedBooking = bookingRepository.save(booking);
            log.info("New booking generated with ID: {}",persistedBooking.getId());
            BookingDto bookingDto = this.modelMapper.map(persistedBooking.getSeat(), BookingDto.class);
            bookingDto.setBookingId(persistedBooking.getId().toString());
            bookingDto.setName(persistedBooking.getName());
            return bookingDto;
        }
        throw new SeatNotFoundException("No Seat not found for given row and column");
    }

    /**
     * Get booking by id
     * @param bookingId
     * @return booking details or throw Exception if the ID is not a valid UUID
     */
    public Optional<BookingDto> fetchBookingById(final String bookingId){
        log.debug("Trying to get booking with id: {}",bookingId);
        Either<Boolean,UUID> isValid = Utils.validateUUID(bookingId);
        if(isValid.isRight()){
            log.info("Fetching booking with id: {}",bookingId);
            Optional<Booking> booking =  bookingRepository.findById(isValid.get());
            return getBookingDto(booking);
        }
        throw new UUIDNotValidException("Not a valid ID");
    }

    /**
     * Get booking based on Row and Column
     * @param row
     * @param column
     * @return booking details
     */
    public Optional<BookingDto> fetchBookingByPosition(final String row, final String column){
        Optional<Booking> booking = bookingRepository.findBySeatRowAndSeatColumn(row, column);
        log.info("Fetching booking with row: {} and column: {}", row, column);
        return getBookingDto(booking);
    }

    /**
     * Check availability for a booking with a given Row and Column
     * @param row
     * @param column
     * @return boolean true/false
     */
    @Transactional(isolation= Isolation.REPEATABLE_READ)
    public boolean checkAvailability(final String row, final String column){
        Optional<Booking> booking = bookingRepository.findBySeatRowAndSeatColumn(row, column);
        log.info("Check availability for a seat with row: {} and column: {}", row, column);
        return booking.isEmpty();
    }


    /**
     * Mapper
     * @param booking
     * @return a BookingDto from a Booking obj
     */
    private Optional<BookingDto> getBookingDto(Optional<Booking> booking) {
        if(booking.isPresent()) {
            BookingDto bookingDto = this.modelMapper.map(booking.get().getSeat(), BookingDto.class);
            bookingDto.setBookingId(booking.get().getId().toString());
            bookingDto.setName(booking.get().getName());
            log.info("Retrieved booking with id: {}", bookingDto.getBookingId());
            return Optional.of(bookingDto);
        }
        return Optional.empty();
    }



}
