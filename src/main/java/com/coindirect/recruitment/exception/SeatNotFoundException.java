package com.coindirect.recruitment.exception;

public class SeatNotFoundException extends RuntimeException {
    public SeatNotFoundException(String exception) {
        super(exception);
    }
}
