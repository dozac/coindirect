package com.coindirect.recruitment.exception;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;

@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomError {

    private String fieldName;
    private String message;

    public CustomError(final String message){
        this.message = message;
    }

    public CustomError(final String fieldName, final String message) {
        this.fieldName = fieldName;
        this.message = message;
    }
}
