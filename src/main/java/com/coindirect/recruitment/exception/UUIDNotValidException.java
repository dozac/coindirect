package com.coindirect.recruitment.exception;

public class UUIDNotValidException extends RuntimeException {
    public UUIDNotValidException(String exception) {
        super(exception);
    }
}
