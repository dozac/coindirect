package com.coindirect.recruitment.exception;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;


@Data
public class ErrorResponse {
    private List<CustomError> errors = new ArrayList<>();
}
