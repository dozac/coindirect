package com.coindirect.recruitment.util;

import io.vavr.control.Either;
import lombok.extern.slf4j.Slf4j;

import java.util.UUID;

@Slf4j
public class Utils {

    /**
     * Validates if a given string is a valid UUID
     * @param valueUUID
     * @return Either obj of two possible types.
     */
    public static Either<Boolean,UUID> validateUUID(final String valueUUID) {
        log.trace("Validate string value of: {} to UUID", valueUUID);
        try {
            UUID uuid = UUID.fromString(valueUUID);
            return Either.right(uuid);
        } catch (IllegalArgumentException exception) {
            return Either.left(false);
        }
    }
}
