package com.coindirect.recruitment.controller;

import com.coindirect.recruitment.dto.BookingDto;
import com.coindirect.recruitment.dto.RequestBookingDto;
import com.coindirect.recruitment.exception.CustomError;
import com.coindirect.recruitment.service.BookingService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;


/**
 * Controller for handling bookings for a bing hall.
 * The hall can be imagined as a grid of rows and columns
 */
@RestController("orders")
public class OrderController {

    private final BookingService bookingService;

    public OrderController(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    /**
     * Creates a booking with the requested details.
     * If unavailable returns a 200 with error message.
     * @param requestBooking the requested booking details.
     * @return on success booking details. on failure error message.
     */
    @PostMapping("create")
    ResponseEntity<?> createBooking(@Valid @RequestBody RequestBookingDto requestBooking){
        if(bookingService.checkAvailability(requestBooking.getRow(), requestBooking.getColumn())){
            return new ResponseEntity<>(bookingService.addBooking(requestBooking), HttpStatus.CREATED);
        }
        return ResponseEntity.ok(new CustomError("Booking unavailable, please try with different Row and Column"));
    }

    /**
     * query a booking by grid position
     * @param row grid position row
     * @param column grid position column
     * @return the booking details. 400 if not found
     */
    @GetMapping("getByPosition/{row}/{column}")
    ResponseEntity<BookingDto> getBookingByPosition(@PathVariable String row , @PathVariable String column){
        Optional<BookingDto> bookingDto = bookingService.fetchBookingByPosition(row, column);
        return bookingDto.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.badRequest().body(null));
    }

    /**
     * query by booking id
     * @param bookingId booking id
     * @return the booking details. 400 if not found
     */
    @GetMapping("getByBookingId/{bookingId}")
    ResponseEntity<BookingDto> getBookingById(@PathVariable String bookingId){
        Optional<BookingDto> bookingDto = bookingService.fetchBookingById(bookingId);
        return bookingDto.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.badRequest().body(null));
    }

    /**
     * Query if a cell is available
     * @param row grid position row
     * @param column grid position column
     * @return true if cell is available. false if not
     */
    @GetMapping("isAvailable/{row}/{column}")
    ResponseEntity<Boolean> isAvailable(@PathVariable String row,@PathVariable String column){
        return ResponseEntity.ok(bookingService.checkAvailability(row, column));
    }

}
