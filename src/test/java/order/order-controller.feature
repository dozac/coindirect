Feature: Booking - Create, Search by id, Search by Row/Column
  Background:
    * url baseUrl

  Scenario: Verify that booking is created with the requested details and status 201
    Given path '/create'
    When request { "name": "string", "row": "A", "column": "2"}
    And method post
    Then status 201
    And print 'Response is: ', response
    And match response == {"bookingId":"#notnull", "row": "A","column": "2","name": "string"}

  Scenario: Verify that If booking unavailable returns a 200 with error message
    Given path '/create'
    When request { "name": "string", "row": "A", "column": "2"}
    And method post
    Then status 200
    And print 'Response is: ', response
    And match response == {"message":"Booking unavailable, please try with different Row and Column"}


  Scenario Outline:  Query a booking by grid position and return the booking details
    Given path '/getByPosition/<row>/<column>'
    And header Accept = 'application/json'
    When method GET
    Then status 200
    And match response == {"bookingId":"#notnull", "row": "A","column": "2","name": "string"}

    Examples:
      | row  | column|
      | A    | 2     |

  Scenario Outline:  Query a booking by grid position if not found return 400
    Given path '/getByPosition/<row>/<column>'
    And header Accept = 'application/json'
    When method GET
    Then status 400
    And match response == ''

    Examples:
      | row  | column|
      | C    | 2     |

  Scenario Outline:  Query a booking by booking id and return the booking details
    #Get existent booking which was already inserted on the above steps so we can use its id
    Given path '/getByPosition/<row>/<column>'
    When method GET
    Then status 200
    Then match response == "#object"
    * def returnedBookingId = response.bookingId
    #Use the bookingId to query the booking
    Given path '/getByBookingId/'+returnedBookingId
    When method GET
    Then status 200
    And print 'Response is: ', response
    Then match response == {"bookingId":"#notnull", "row": "A","column": "2","name": "string"}
    Then match returnedBookingId == response.bookingId

    Examples:
      | row  | column|
      | A    | 2     |

  Scenario: Query a booking by ID if not found return 400
    * def bookingId = 'f0e5a01e-0bb2-49b4-9750-a0b0bf9d'
    Given path '/getByBookingId/'+bookingId
    And header Accept = 'application/json'
    When method GET
    Then status 400
    And match response == ''

  Scenario Outline:  Query if a cell is available and return boolean true
    Given path '/isAvailable/<row>/<column>'
    And header Accept = 'application/json'
    When method GET
    Then status 200
    And print 'Response is: ', response
    And match response == 'true'

    Examples:
      | row  | column|
      | B   | 1     |

  Scenario Outline:  Query if a cell is unavailable and return boolean false
    Given path '/isAvailable/<row>/<column>'
    And header Accept = 'application/json'
    When method GET
    Then status 200
    And print 'Response is: ', response
    And match response == 'false'

    Examples:
      | row  | column|
      | A    | 2     |

  Scenario: Query a booking by ID where the id is not a valid UUID
    * def bookingId = 'f0e5a01e'
    Given path '/getByBookingId/'+bookingId
    And header Accept = 'application/json'
    When method GET
    Then status 400
    Then match response == "#object"
    * def errorMessage = response.message
    And print 'Response is: ', response
    And match errorMessage == 'Not a valid ID'

