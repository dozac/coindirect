package order;

import com.intuit.karate.junit5.Karate;

public class BookingSystemRunner {

    @Karate.Test
    Karate testAll() {
        return Karate.run().relativeTo(getClass());
    }

}
