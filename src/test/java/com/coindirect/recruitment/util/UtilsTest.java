package com.coindirect.recruitment.util;

import io.vavr.control.Either;
import org.junit.jupiter.api.Test;
import java.util.UUID;
import org.junit.jupiter.api.DisplayName;
import static org.junit.jupiter.api.Assertions.*;

class UtilsTest {

    @Test
    @DisplayName("Test if String is a valid UUID")
    void validate_UUID_If_Valid() {
        Either<Boolean, UUID> result  = Utils.validateUUID("41417fd0-45be-432a-845f-4da2c96d4e0");
        assertTrue(result.isRight());
    }

    @Test
    @DisplayName("Test if String is not a valid UUID")
    void validate_UUID_If_Not_Valid() {
        Either<Boolean, UUID> result  = Utils.validateUUID("41417fd0-45be");
        assertTrue(result.isLeft());
    }
}
